////////////////////////////////////////////
/// BlockOS default skinning definitions ///
////////////////////////////////////////////
/// By Teo "Nullable"                    ///
////////////////////////////////////////////
/// Not original enough to be            ///
/// copyrighted, thus public domain      ///
////////////////////////////////////////////

BlockOS_registerSkinnable("BlockOS_TaskBar", "image", "Add-Ons/System_BlockOS/ui/Main");
BlockOS_registerSkinnable("BlockOS_StartMenuButton", "button", "Add-Ons/System_BlockOS/ui/Main");
BlockOS_registerSkinnable("BlockOS_StartMenu", "image", "Add-Ons/System_BlockOS/ui/Main");
BlockOS_registerSkinnable("BlockOS_Logo", "image", "Add-Ons/System_BlockOS/ui/Main");
