// BlockOS
// Background Module
// Author: Fluffy

function BlockOS_BackgroundGui::onWake(%this) {
    setmodpaths(getmodpaths());
    
    for (%i = 0;%i < BlockOS_Background_Scroll.getCount();%i++) 
    {
        BlockOS_Background_Scroll.getObject(%i).delete();
    }
    
    BlockOS_Background_Scroll.clear();
    
    %path = "config/BOS/Wallpapers/*.jpg";
    
    for (%i = findFirstFile(%path);strLen(%i) > 0;%i = findNextFile(%path)) 
    {
        %ctrl = new GuiBitmapCtrl() 
        {
            incrementsTotalHeight = true;
            bitmap = getSubStr(%i, 0, strLen(%i) - 4);
            extent = 161 SPC 125;
            position = 0 SPC (BlockOS_Background_Scroll.getCount() / 2) * 130;
        };
        
        %overlay = new GuiBitmapButtonCtrl() {
            incrementsTotalHeight = false;
            extent = %ctrl.extent;
            position = %ctrl.position;
            command = "BlockOS_Background_Use(" @ %ctrl.getID() @ ");";
        };
        
        BlockOS_Background_Scroll.add(%ctrl);
        BlockOS_Background_Scroll.add(%overlay);
    }
    %last = BlockOS_Background_Scroll.getObject(BlockOS_Background_Scroll.getCount() - 1);
    
    BlockOS_Background_Scroll.extent = 164 SPC getWord(%last.position, 1) + getWord(%last.extent, 1);
    
    // And for Skins --------------------------------------------------
    
    %path = "";
    %ctrl = "";
    %overlay = "";
    %last = "";
    %i = "";
    
    for (%i = 0;%i < BlockOS_Settings_SkinSwatch.getCount();%i++) 
    {
        BlockOS_Settings_SkinSwatch.getObject(%i).delete();
    }
    
    BlockOS_Settings_SkinSwatch.clear();
    
    %path = "config/BOS/Skins/*.zip";
    
    for (%i = findFirstFile(%path);strLen(%i) > 0;%i = findNextFile(%path)) 
    {
        %iconFile = "config/BOS/Skins/" @ fileBase(%i) @ "/skin.png";
        %ctrl = new GuiBitmapCtrl() 
        {
            incrementsTotalHeight = true;
            bitmap = %iconFile;
            extent = 160 SPC 130;
            position = 0 SPC (BlockOS_Settings_SkinSwatch.getCount() / 2) * 135;
        };
        
        %overlay = new GuiBitmapButtonCtrl() {
            incrementsTotalHeight = false;
            extent = %ctrl.extent;
            position = %ctrl.position;
            command = "$BlockOS::Settings::SelectedSkin = \"" @ fileBase(%i) @ "\";";
        };
        
        BlockOS_Settings_SkinSwatch.add(%ctrl);
        BlockOS_Settings_SkinSwatch.add(%overlay);
    }
    %last = BlockOS_Settings_SkinSwatch.getObject(BlockOS_Settings_SkinSwatch.getCount() - 1);
    
    BlockOS_Settings_SkinSwatch.extent = 162 SPC getWord(%last.position, 1) + getWord(%last.extent, 1);
}

function BlockOS_Background_Use(%ctrl)
{
   BlockOS_Background_preview.setBitmap(%ctrl.bitmap);
   $BlockOS::Settings::Bitmap = %ctrl.bitmap;
}

function BlockOS_Background_Save()
{
   canvas.popDialog(BlockOS_BackgroundGui);
    if($BlockOS::Save::LogoVis)
   {
      BlockOS_Logo.setVisible(1);
   }
   else
   {
      BlockOS_Logo.setVisible(0);
   }
   
   if(strLen($BlockOS::Settings::Bitmap) > 1)
   {
      BlockOS_BGImage.setBitmap($BlockOS::Settings::Bitmap);
      $BlockOS::Save::BGPicture = $BlockOS::Settings::Bitmap;
   }
   export("$BlockOS::Save*", "Config/BOS/prefs.cs");

}
   

//--- OBJECT WRITE BEGIN ---
new GuiControl(BlockOS_BackgroundGui) {
   profile = "GuiDefaultProfile";
   horizSizing = "right";
   vertSizing = "bottom";
   position = "0 0";
   extent = "640 480";
   minExtent = "8 2";
   visible = "1";
   accelerator = "escape";

   new GuiWindowCtrl() {
      profile = "BOSWindowProfile";
      horizSizing = "right";
      vertSizing = "bottom";
      position = "117 47";
      extent = "413 368";
      minExtent = "8 2";
      visible = "1";
      command = "canvas.popDialog(BlockOS_BackgroundGui);";
      accelerator = "escape";
      text = "BlockOS Settings";
      maxLength = "255";
      resizeWidth = "0";
      resizeHeight = "0";
      canMove = "1";
      canClose = "1";
      canMinimize = "0";
      canMaximize = "0";
      minSize = "50 50";
      closeCommand = "canvas.popDialog(BlockOS_BackgroundGui);";

      new GuiSwatchCtrl(BlockOS_Settings_BackgroundPane) {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "4 70";
         extent = "405 294";
         minExtent = "8 2";
         visible = "0";
         color = "150 150 150 255";

         new GuiSwatchCtrl() {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "bottom";
            position = "216 10";
            extent = "177 270";
            minExtent = "8 2";
            visible = "1";
            color = "255 255 255 255";

            new GuiScrollCtrl() {
               profile = "GuiScrollProfile";
               horizSizing = "right";
               vertSizing = "center";
               position = "0 0";
               extent = "177 270";
               minExtent = "8 2";
               visible = "1";
               willFirstRespond = "0";
               hScrollBar = "alwaysOff";
               vScrollBar = "alwaysOn";
               constantThumbHeight = "0";
               childMargin = "0 0";
               rowHeight = "40";
               columnWidth = "30";

               new GuiSwatchCtrl(BlockOS_Background_Scroll) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "0 0";
                  extent = "164 0";
                  minExtent = "8 2";
                  visible = "1";
                  color = "0 0 0 0";
               };
            };
         };
         new GuiSwatchCtrl() {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "bottom";
            position = "12 10";
            extent = "198 270";
            minExtent = "8 2";
            visible = "1";
            color = "255 255 255 255";

            new GuiMLTextCtrl() {
               profile = "GuiMLTextProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "10 10";
               extent = "168 56";
               minExtent = "8 2";
               visible = "1";
               lineSpacing = "2";
               allowColorChars = "0";
               maxChars = "-1";
               text = "Here you may choose any image from your Wallpapers folder (config/BOS/Wallpapers) to be your BlockOS Background!";
               maxBitmapHeight = "-1";
               selectable = "1";
            };
            new GuiCheckBoxCtrl() {
               profile = "GuiCheckBoxProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "9 62";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               variable = "$BlockOS::Save::LogoVis";
               text = "Show logo";
               groupNum = "-1";
               buttonType = "ToggleButton";
            };
            new GuiBitmapCtrl(BlockOS_Background_preview) {
               profile = "GuiDefaultProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "9 85";
               extent = "181 150";
               minExtent = "8 2";
               visible = "1";
               bitmap = "";
               wrap = "0";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               keepCached = "0";
            };
         };
      };
      new GuiSwatchCtrl(BlockOS_Settings_SettingsPane) {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "4 70";
         extent = "405 294";
         minExtent = "8 2";
         visible = "1";
         color = "150 150 150 255";

         new GuiSwatchCtrl() {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "bottom";
            position = "12 10";
            extent = "198 270";
            minExtent = "8 2";
            visible = "1";
            color = "255 255 255 255";

            new GuiBitmapButtonCtrl() {
               profile = "HudInvTextProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "14 17";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               command = "keyGui::changePrompt();";
               text = "Change Key";
               groupNum = "-1";
               buttonType = "PushButton";
               bitmap = "Add-Ons/System_BlockOS/ui/Main/menubutton";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               mKeepCached = "0";
               mColor = "255 255 255 255";
            };
            new GuiBitmapButtonCtrl() {
               profile = "HudInvTextProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "14 57";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               command = "canvas.pushDialog(regNameGui);";
               text = "Change Name";
               groupNum = "-1";
               buttonType = "PushButton";
               bitmap = "Add-Ons/System_BlockOS/ui/Main/menubutton";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               mKeepCached = "0";
               mColor = "255 255 255 255";
            };
            new GuiBitmapButtonCtrl() {
               profile = "HudInvTextProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "14 97";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               command = "Canvas.pushDialog(AvatarGui);";
               text = "Change Avatar";
               groupNum = "-1";
               buttonType = "PushButton";
               bitmap = "Add-Ons/System_BlockOS/ui/Main/menubutton";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               mKeepCached = "0";
               mColor = "255 255 255 255";
            };
            new GuiBitmapButtonCtrl() {
               profile = "HudInvTextProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "14 137";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               command = "Canvas.pushDialog(optionsDlg);";
               text = "Blockland Options";
               groupNum = "-1";
               buttonType = "PushButton";
               bitmap = "Add-Ons/System_BlockOS/ui/Main/menubutton";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               mKeepCached = "0";
               mColor = "255 255 255 255";
            };
            new GuiCheckBoxCtrl() {
               profile = "GuiCheckBoxProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "14 180";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               variable = "$BlockOS::Save::24Toggle";
               text = "24 Hour Time";
               groupNum = "-1";
               buttonType = "ToggleButton";
            };
         };
         new GuiSwatchCtrl() {
            profile = "GuiDefaultProfile";
            horizSizing = "right";
            vertSizing = "bottom";
            position = "216 10";
            extent = "177 270";
            minExtent = "8 2";
            visible = "1";
            color = "255 255 255 255";

            new GuiScrollCtrl() {
               profile = "GuiScrollProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "0 0";
               extent = "177 200";
               minExtent = "8 2";
               visible = "1";
               willFirstRespond = "0";
               hScrollBar = "alwaysOff";
               vScrollBar = "alwaysOn";
               constantThumbHeight = "0";
               childMargin = "0 0";
               rowHeight = "40";
               columnWidth = "30";

               new GuiSwatchCtrl(BlockOS_Settings_SkinSwatch) {
                  profile = "GuiDefaultProfile";
                  horizSizing = "right";
                  vertSizing = "bottom";
                  position = "1 1";
                  extent = "162 0";
                  minExtent = "8 2";
                  visible = "1";
                  color = "0 0 0 0";
               };
            };
            new GuiBitmapButtonCtrl() {
               profile = "HudInvTextProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "14 219";
               extent = "140 30";
               minExtent = "8 2";
               visible = "1";
               command = "BlockOS_setSkin($BlockOS::Settings::SelectedSkin);";
               text = "Choose Skin";
               groupNum = "-1";
               buttonType = "PushButton";
               bitmap = "Add-Ons/System_BlockOS/ui/Main/menubutton";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               mKeepCached = "0";
               mColor = "255 255 255 255";
            };
         };
      };
      new GuiBitmapButtonCtrl() {
         profile = "HudInvTextProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "287 40";
         extent = "109 30";
         minExtent = "8 2";
         visible = "1";
         command = "BlockOS_Background_Save();";
         text = "Save";
         groupNum = "-1";
         buttonType = "PushButton";
         bitmap = "Add-Ons/System_BlockOS/ui/main/tab";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         mKeepCached = "0";
         mColor = "255 255 255 255";
      };
      new GuiBitmapButtonCtrl(Settings_BackgroundTab) {
         profile = "HudInvTextProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "125 40";
         extent = "109 30";
         minExtent = "8 2";
         visible = "1";
         command = "BlockOS_Settings_SettingsPane.setVisible(0); BlockOS_Settings_BackgroundPane.setVisible(1); Settings_BackgroundTab.setBitmap(\"Add-ons/System_BlockOS/ui/main/tabUse\"); Settings_GeneralTab.setBitmap(\"Add-ons/System_BlockOS/ui/main/tab\");";
         text = "Background";
         groupNum = "-1";
         buttonType = "PushButton";
         bitmap = "Add-Ons/System_BlockOS/ui/main/tab";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         mKeepCached = "0";
         mColor = "255 255 255 255";
      };
      new GuiBitmapButtonCtrl(Settings_GeneralTab) {
         profile = "HudInvTextProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "15 40";
         extent = "109 30";
         minExtent = "8 2";
         visible = "1";
         command = "BlockOS_Settings_SettingsPane.setVisible(1); BlockOS_Settings_BackgroundPane.setVisible(0); Settings_GeneralTab.setBitmap(\"Add-ons/System_BlockOS/ui/main/tabUse\"); Settings_BackgroundTab.setBitmap(\"Add-ons/System_BlockOS/ui/main/tab\");";
         text = "General";
         groupNum = "-1";
         buttonType = "PushButton";
         bitmap = "Add-Ons/System_BlockOS/ui/main/tabUse";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         mKeepCached = "0";
         mColor = "255 255 255 255";
      };
   };
};
//--- OBJECT WRITE END ---


if(!isFile("config/BOS/Wallpapers/1.jpg"))
{
   if(!isFile("config/BOS/Wallpapers/info.txt"))
   {
      %fo = new FileObject();
      %fo.openForWrite("config/BOS/Wallpapers/info.txt");
   
      %fo.writeLine("Wallpaper Help");
      %fo.writeLine("");
      %fo.writeLine("BlockOS Wallpaper must be in this folder and be in JPEG format to be recognised");
   
      %fo.close();
      %fo.delete();
   }
   fileCopy("./ui/WallpaperArchive/1.jpg", "config/BOS/Wallpapers/1.jpg");
   fileCopy("./ui/WallpaperArchive/2.jpg", "config/BOS/Wallpapers/2.jpg");
   fileCopy("./ui/WallpaperArchive/3.jpg", "config/BOS/Wallpapers/3.jpg");
   fileCopy("./ui/WallpaperArchive/4.jpg", "config/BOS/Wallpapers/4.jpg");
   

   $BlockOS::Save::BGPicture = "config/BOS/Wallpapers/3.jpg";

   if($BlockOS::Save::LogoVis $= "")
   {
      $BlockOS::Save::LogoVis = 1;
   }
}

if(!isFile("config/BOS/Skins/default.zip"))
{
    %fo = new FileObject();
    %fo.openForWrite("config/BOS/Skins/info.txt");
   
    %fo.writeLine("BlockOS skins must be in this folder to be recognised");
   
    %fo.close();
    %fo.delete();

    fileCopy("./ui/Skin.zip", "config/BOS/Skins/default.zip");
}
