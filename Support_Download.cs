// +====================================================+
// | The BlockOS Project
// | 
// |  By Brian Smith, Fluffy and Nullable
// +====================================================+

exec("./ProgressBar.gui");
function createDownloaderConnection(%file,%host,%download, %needsRestart)
{
    $download = %download;
    %tcp = new TCPObject(Downloader);
    %tcp.file = "/" @ %file;
    %tcp.host = %host;
    %tcp.needsRestart = %needsRestart;
    %tcp.connect(%tcp.host @ ":80");
}

function Downloader::onConnected(%this)
{
        %this.send("GET " @ %this.file @ " HTTP/1.0\nHost: " @ %this.host @ "\r\nUser-Agent: Torque/1.0\r\n\r\n");
}

function Downloader::onLine(%this, %line)
{
    if(strPos(%line, "Content-Length:") >= 0)
    {
        %this.length = getword(%line, 1);
    }
    if(%line $= "")
    {
        %this.setBinarySize(%this.length);
        canvas.pushDialog(ProgressBar);
    }
}

function Downloader::onBinChunk(%this, %chunk)
{
    if(%chunk >= %this.length) {
        if(isWriteableFilename($download)) {
            %this.saveBufferToFile($download);
            %this.disconnect();
            %msgBoxContent = "You have downloaded " @ $download @ " successfully.";
            %msgBoxWhenOK = "";
            if (%this.needsRestart) {
                %msgBoxContent = %msgBoxContent NL "Restarting to finish install.";
                %msgBoxWhenOK = %msgBoxWhenOK @ "quit();";
            }
            messageBoxOK("Success", %msgBoxContent, %msgBoxWhenOK);
            LoadAppMenu();
        } else
            messageBoxOK("Failure","Downloading your file wasn't successful.");
        $download = "";
        canvas.popDialog(ProgressBar);
    } else {
        ProgressText.setvalue("Downloaded: " @ mFloor((%chunk/%this.length)*100) @ "%");
        ProgressBar2.setValue(%chunk/%this.length);
    }
}
