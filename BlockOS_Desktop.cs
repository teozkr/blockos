// +====================================================+
// | The BlockOS Project
// | 
// |  By Brian Smith, Fluffy and Nullable
// +====================================================+

$BlockOS::Desktop::GridsizeX = 80;
$BlockOS::Desktop::GridsizeY = 90;

if (!isObject("BlockOS_Desktop"))
    exec("./BlockOS_Desktop.gui");

if (!$BlockOS::Desktop::Initialized) {
    $BlockOS::Desktop::Initalized = true;
    $BlockOS::Desktop::ShortcutCount = 0;
}

function BlockOS_getShortcutLocation(%id, %offsetX, %offsetY) {
    %deskSize = BlockOS_Desktop.extent;
    %bottomBarSize = getWord(%deskSize, 1) - getWord(BlockOS_BottomContainer.position, 1); // Y of BottomContainer
    %deskSizeX = mFloor((getWord(%deskSize, 0) - %offsetX * 2) / $BlockOS::Desktop::GridsizeX);
    %deskSizeY = mFloor((getWord(%deskSize, 1) - %offsetY * 2 - %bottomBarSize) / $BlockOS::Desktop::GridsizeY);

    %posX = %id % %deskSizeX;
    %posY = mFloor(%id / %deskSizeX);

    return %posX * $BlockOS::Desktop::GridsizeX + %offsetX SPC %posY * $BlockOS::Desktop::GridsizeY + %offsetY;
}

function BlockOS_rebuildDesktop() {
    for (%id = 0;%id < $BlockOS::Desktop::ShortcutCount;%id++) {
        if (isObject($BlockOS::Desktop::Shortcut[%id, icon]))
            $BlockOS::Desktop::Shortcut[%id, icon].delete();
        if (isObject($BlockOS::Desktop::Shortcut[%id, labelObj]))
            $BlockOS::Desktop::Shortcut[%id, labelObj].delete();
        %iconName = $BlockOS::Desktop::Shortcut[%id, iconName];
        %iconFolder = $BlockOS::Desktop::Shortcut[%id, iconFolder];
        %command = $BlockOS::Desktop::Shortcut[%id, command];
        %label = $BlockOS::Desktop::Shortcut[%id, label];
        %pos = BlockOS_getShortcutLocation(%id, 20, 20);
        %icon = new GuiBitmapButtonCtrl("BlockOS_Shortcut_" @ %iconName) {
            profile = "GuiDefaultProfile";
            horizSizing = "center";
            vertSizing = "center";
            position = getWord(%pos, 0) + 15 SPC getWord(%pos, 1);
            extent = "39 39";
            visible = true;
            command = %command;
            text = " ";
            groupNum = -1;
            buttonType = "PushButton";
            bitmap = %iconFolder @ %iconName;
            lockAspectRatio = false;
            alignLeft = false;
            overflowImage = false;
            mKeepCached = false;
            mColor = 255 SPC 255 SPC 255 SPC 255;
        };
        %labelObj = new GuiTextCtrl() {
            profile = "HudInvTextProfile";
            horizSizing = "center";
            vertSizing = "center";
            position = getWord(%pos, 0) SPC getWord(%pos, 1) + 50;
            extent = "72 18";
            visible = true;
            text = %label;
            maxLength = 255;
        };
        BlockOS_registerSkinnable("BlockOS_Shortcut_" @ %iconName, "button", %iconFolder, %iconName);
        BlockOS_Desktop.add(%icon);
        BlockOS_Desktop.add(%labelObj);
        $BlockOS::Desktop::Shortcut[%id, icon] = %icon;
        $BlockOS::Desktop::Shortcut[%id, labelObj] = %labelObj;
    }
}

function BlockOS_registerShortcut(%gui, %label, %iconName, %iconFolder) {
    if ($BlockOS::Desktop::ShortcutByGUI[%gui] $= "") {
        %id = BlockOS_registerShortcutRaw("canvas.pushDialog(" @ %gui @ ");", %label, %iconName, %iconFolder);
        if (%id != -1) {
            $BlockOS::Desktop::ShortcutByGUI[%gui] = %id;
        }
        return %id;
    } else {
        error("Shortcut is already declared, ignoring.");
        return -1;
    }
}

function BlockOS_registerShortcutRaw(%command, %label, %iconName, %iconFolder) {
    %id = $BlockOS::Desktop::ShortcutCount;
    $BlockOS::Desktop::ShortcutCount++;
    if (getSubStr(%iconFolder, strLen(%iconFolder - 1), 1) !$= "/")
        %iconFolder = %iconFolder @ "/";
    $BlockOS::Desktop::Shortcut[%id, iconName] = %iconName;
    $BlockOS::Desktop::Shortcut[%id, iconFolder] = %iconFolder;
    $BlockOS::Desktop::Shortcut[%id, command] = %command;
    $BlockOS::Desktop::Shortcut[%id, label] = %label;
    if ($BlockOS::Loaded)
        BlockOS_rebuildDesktop();
    return %id;
}

function BlockOS_PoopCredits()
{
   messageBoxOK("Credits","Scripting:\n- Brian Smith\n- Fluffy\n\nGraphics/Web Design:\n- Fluffy\n\nOnline Components/Debugging:\n- Nullable\n- Brian Smith");
}

//In-Game Keybind
if (!$BlockOS::Binds)
{
	$remapDivision[$remapCount] = "BlockOS";
	$remapName[$remapCount] = "In-Game BlockOS";
	$remapCmd[$remapCount] = "BlockOS_PushInGame";
	$remapCount++;
	$BlockOS::Binds = true;
}

function BlockOS_PushInGame(%down)
{
    if (!%down) return;

    if(!isObject(serverConnection))
        return;
      
	if(!BlockOS_Desktop.isAwake())
	{
		canvas.pushDialog(BlockOS_Desktop);
		BlockOS_BackgroundSwatch.setVisible(1);
		BlockOS_BGImage.setVisible(0);
		return;
	}
	canvas.popDialog(BlockOS_Desktop);
	BlockOS_BackgroundSwatch.setVisible(0);
	BlockOS_BGImage.setVisible(1);
}

package BlockOS_Disconnect
{
   function disconnect()
   {
      parent::disconnect();
      
	   if(!BlockOS_Desktop.isAwake())
	   {
		   canvas.pushDialog(BlockOS_Desktop);
		   BlockOS_BackgroundSwatch.setVisible(0);
		   BlockOS_BGImage.setVisible(1);
		   return;
	   }
   }
   
   function GameConnection::onConnectionDropped(%x, %y, %z)
   {
      parent::onConnectionDropped(%x, %y, %z);
      
      if(!BlockOS_Desktop.isAwake())
	   {
		   canvas.pushDialog(BlockOS_Desktop);
		   BlockOS_BackgroundSwatch.setVisible(0);
		   BlockOS_BGImage.setVisible(1);
		   return;
	   }
   }
};
activatePackage(BlockOS_Disconnect);

function BlockOS_DismissNew()
{
   canvas.popDialog(BlockOS_New);
   $BlockOS::Save::FirstRun = $BlockOS::CurrentVersion;
   export("$BlockOS::Save*", "Config/BOS/prefs.cs");
}

package BlockOS_Desktop_ResolutionChange {
    function optionsDlg::applyGraphics(%this) {
        %retval = parent::applyGraphics(%this);
        BlockOS_rebuildDesktop();
        return %retval;
    }
};
activatePackage(BlockOS_Desktop_ResolutionChange);