function ExecAllApps() {
        discoverFile("config/BlockOS/Apps/*");
        echo("Executing All Applications.");
        $BlockOS::AppCount += 0; // Force to int
        %path = "config/BOS/Apps/"; 
        for (%filesearch = findFirstFile(%path @ "*.zip");strlen(%filesearch) > 0;%filesearch = findNextFile(%path @ "*.zip")) {
                %app = getsubstr(%filesearch, strlen(%path), strlen(%filesearch)-strlen(%path)-4);
                if ($BlockOS::LoadedApp[%app])
                    continue;
                exec("config/BOS/Apps/" @ %app @ "/main.cs");
                %name = %app;
                if (isFile("config/BOS/Apps/" @ %app @ "/name.txt")) {
                    %namefile = new FileObject();
                    %namefile.openForRead("config/BOS/Apps/" @ %app @ "/name.txt");
                    %name = trim(%namefile.readLine());
                    %namefile.close();
                    %namefile.delete();
                }
                %version = 0;
                if (isFile("config/BOS/Apps/" @ %app @ "/version.txt")) {
                    %verfile = new FileObject();
                    %verfile.openForRead("config/BOS/Apps/" @ %app @ "/version.txt");
                    %version = trim(%verfile.readLine());
                    %verfile.close();
                    %verfile.delete();
                }
                $BlockOS::App[$BlockOS::AppCount] = %app;
                $BlockOS::AppName[$BlockOS::AppCount] = %name;
                $BlockOS::AppVersion[$BlockOS::AppCount] = %version;
                $BlockOS::AppReverse[%app] = $BlockOS::AppCount;
                $BlockOS::AppCount += 1;
                $BlockOS::LoadedApp[%app] = true;
        }
}

function LoadAppMenu() {
    setModPaths(getModPaths());
    echo("Loading App Menu.");
    ExecAllApps();
    if($BlockOS::AppCount > 0) {
        for(%i=0;%i<$BlockOS::AppCount;%i++) {
            %app = $BlockOS::App[%i];
            %name = $BlockOS::AppName[%i];
            %gui = %app @ "GUI";
            if(%app !$= "" && isObject(%gui) && isFile("config/BOS/Apps/" @ %app @ ".zip")) {
                BlockOS_registerShortcut(%gui, %name, %app, "config/BOS/Apps/" @ %app);
            }
        }
    }
}
