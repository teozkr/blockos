package BOS_Auth_Interception {
    function authTCPobj_Client::onDisconnect(%this) {
        parent::onDisconnect(%this);
        if(!isUnlocked())
            return messageBoxOK("Uh-Oh","BlockOS can't authenticate if you aren't authenticated properly against the master server!");
        if(!isObject(BOSAuthTCP))
            new TCPObject(BOSAuthTCP);
        BOSAuthTCP.connect("blockos.nullable.se:80");
    }
};
activatePackage(BOS_Auth_Interception);

function BOSAuthTCP::onConnected(%this)
{
        %name = $Pref::Player::NetName;
        %csrftoken = "lol";

        %body = "";
        %body = %body @ "----boundary\r\n";
        %body = %body @ "Content-Disposition: form-data; name=\"csrfmiddlewaretoken\"\r\n";
        %body = %body @ "\r\n";
        %body = %body @ %csrftoken @ "\r\n";
        %body = %body @ "----boundary\r\n";
        %body = %body @ "Content-Disposition: form-data; name=\"name\"\r\n";
        %body = %body @ "\r\n";
        %body = %body @ %name @ "\r\n";
        %body = %body @ "----boundary--\r\n";

        %head = "";
        %head = %head @ "POST /blauth HTTP/1.0\r\n";
        %head = %head @ "Host: blockos.nullable.se\r\n";
        %head = %head @ "User-Agent: Torque/1.0\r\n";
        %head = %head @ "Content-Type: multipart/form-data; boundary=--boundary\r\n";
        %head = %head @ "Content-Length:" SPC strLen(%body) @ "\r\n";
        %head = %head @ "Cookie: csrftoken=" @ %csrftoken @ "\r\n";

        %msg = %head @ "\r\n" @ %body;

        %this.send(%msg);
}
