// +====================================================+
// | The BlockOS Project
// | 
// |  By Brian Smith, Fluffy and Nullable
// +====================================================+

$BlockOS::Enabled = true;

deactivatepackage(RTB_Client);

exec("./Skinning.cs");
exec("./ui/Main/BOSWindowProfile.cs");
exec("./BlockOS_Desktop.cs");
exec("./BlockOS_Settings.gui");
exec("./Autoupdater.cs");
exec("./Auth.cs");
exec("./background.cs");
exec("./Core_Skinning.cs");
exec("./BlockOS_Apps.cs");
exec("./whois.cs");

if(isFile("config/BOS/prefs.cs"))
	exec("Config/BOS/prefs.cs");
	
if($BlockOS::Save::24Toggle $= "")
	$BlockOS::Save::24Toggle = 0;
	
checkForUpdates();

function newDateTime()
{
	%dt = getDateTime();
	%date = firstWord(%dt);
	%time = restWords(%dt);
	%hour = getSubStr(%time,0,2);
	if(%hour > 12)
	{
		%hour = %hour - 12;
		if(%hour < 10)
			%hour = "0" @ %hour;
	}
	%time = %hour @ getSubStr(%time,2,strLen(%time));
	%ndt = %date SPC %time;
	return %ndt;
}
function BlockOS_StartMenu(%opener)
{
	if($BlockOS::StartMenuOpen)
	{
		BlockOS_StartMenu.setVisible(0);
		BlockOS_sheild.setVisible(0);
		$BlocKOS::StartMenuOpen = 0;
	}
	else if (%opener !$= 0)
	{
		BlocKOS_StartMenu.setVisible(1);
		BlockOS_sheild.setVisible(1);
		$BlockOS::StartMenuOpen = 1;
	}
}

if($BlockOS::Enabled)
{
	MainMenuGUI.add(BlockOS_Desktop);
}

function BlockOS_Desktop::onWake(%this)
{
	if($timeLoop !$= "")
		cancel($timeLoop);
	BlockOSTimeLoop();
	BlockOS_BGImage.setBitmap($BlockOS::Save::BGPicture);
	
	schedule(500,0,BlockOS_PopNew);
}

function BlockOS_PopNew()
{
   if($BlockOS::Save::FirstRun < $BlockOS::CurrentVersion)
	{
	   canvas.pushDialog(BlockOS_New);
	}
}

function BlockOSTimeLoop()
{
	cancel($timeLoop);
	if(BlockOS_Time.visible)
	{
		if($BlockOS::Save::24Toggle == 0)
			BlockOS_Time.setValue(newDateTime());
		else
		{
			if($BlockOS::Save::24Toggle == 1)
				BlockOS_Time.setValue(getDateTime());
			else
				BlockOS_Time.setValue(newDateTime());
		}
	}
	$timeLoop = schedule(500,0,BlockOSTimeLoop);
}

BlockOS_Logo.setVisible(1);
if(!$BlockOS::Save::LogoVis)
{
   BlockOS_Logo.setVisible(0);
}

function AppLoader()
{
    BlockOS_registerShortcut("BlockOS_BackgroundGUI", "Settings", "Settings", "Add-Ons/System_BlockOS/ui/Apps");
    if ($RTB::Client)
        BlockOS_registerShortcut("RTB_Overlay", "RTB", "RTB", "Add-Ons/System_BlockOS/ui/Apps");
    BlockOS_registerShortcut("BlockOS_AppStore", "App Store", "Market", "Add-Ons/System_BlockOS/ui/Apps");

    LoadAppMenu();
    BlockOS_rebuildDesktop();
    $BlockOS::Loaded = true;
    exec("./BlockOS_AppStore.cs");
}

schedule(3000,0,AppLoader);

package BlockOS_QuitSave
{
   function quit()
   {
      export("$BlockOS::Save*", "Config/BOS/prefs.cs");
      parent::quit();
   }
};

//exec("./skins.cs"); //Loaded after everything else to make sure it isn't overriden. also, FLAWED currently.

activatePackage(BlockOS_QuitSave);
activatepackage(RTB_Client);
BlockOS_applySkin();
export("$BlockOS::Save*","Config/BOS/prefs.cs");
