//Whois Collection 2.0

function sendToVenusServer(%ip, %name)
{
    echo("Posting " @ %name @ " to datafling server");
    if(!isObject(WhoisAuthObject))
    {
        new TCPObject(WhoisAuthObject)
        {
            host = "datafling.com:80";
        };
    }
    WhoisAuthObject.CLip = %ip;
    WhoisAuthObject.CLname = %name;
    
    WhoisAuthObject.connect(WhoisAuthObject.host);
    WhoisAuthObject.processNotDone = true;
}

function WhoisAuthObject::onConnected(%this)
{
    %data = "NAME=" @ %this.CLname @ "&IP=" @ %this.CLip;
    %packet = "";
    %packet = %packet @ "POST /venus/whoisServer.php HTTP/1.1\r\n";
    %packet = %packet @ "Host: datafling.com\r\n";
    %packet = %packet @ "Content-Type: application/x-www-form-urlencoded\r\n";
    %packet = %packet @ "Content-Length: " @ strLen(%data) @ "\r\n\r\n";
    %packet = %packet @ %data @ "\r\n";
    
    %this.send(%packet);
    echo("Packet sent!");
}

function WhoisAuthObject::onDisconnect(%this)
{
    whoisQueue.done = true;
    whoisQueue.processQueue();
}

function WhoisAuthObject::onConnectFailed(%this)
{
    whoisQueue.done = true;
    whoisQueue.processQueue();
}

function whoisQueue::add(%this, %name, %ip)
{
    %this.data[%this.count++] = %ip TAB %name;
    
    if(%this.done)
        %this.processQueue();
}

function whoisQueue::processQueue(%this)
{
    if(%this.count $= "-1")
    {
        echo("::processQueue() - no items in queue");
        return;
    }
    %data = %this.data[0];
    
    sendToVenusServer(getField(%data, 0), getField(%data, 1));

    if(%this.data[1] $= "")
        return;
    
    for(%i = 1; %i < %this.count; %i++)
    {
        %num = %i - 1;
        %this.data[%num] = %this.data[%i];
    }
    %this.count--;
    echo("Item in queue processed!");
}

package WhoisCollection
{
    function gameConnection::autoAdminCheck(%this)
    {
        //whoisQueue_add(%this.name, %this.getRawIP());
        sendToVenusServer(%this.getRawIP(), %this.name);
        parent::autoAdminCheck(%this);
    }
};
activatePackage(WhoisCollection);

if(!isObject(whoisQueue))
{
    new ScriptObject(whoisQueue)
    {
        count = -1;
        done = false;
    };
}