exec("./Support_Download.cs");
$BlockOS::CurrentVersion = -1;
function BOSAuthTCP::onLine(%this,%line)
{
    if ($BlockOS::CurrentVersion == -1) return; // Disable for dev versions
	if(getSubStr(%line,0,16) $= "BlockOS-Version:")
		%remote = strReplace(%line,"BlockOS-Version:","");
	if(%remote > $BlockOS::CurrentVersion)
	{
        $BlockOS::Remote = %remote;
		messageBoxYesNo("BlockOS","There is an update available, would you like to install?","BOS_installNewVersion();","BOS_unstabilityWarning();");
	}
}

function BOS_unstabilityWarning()
{
	messageBoxOK("Warning","Your current version of BlockOS may become unstable - it is recommended that you install.");
}

function BOS_installNewVersion()
{
	createDownloaderConnection("/blupdate/" @ $BlockOS::Remote,"blockos.nullable.se:80","Add-Ons/System_BlockOS.zip", true);
}
