//BlockOS AppStore

exec("./Support_Download.cs");
if (!isObject(BlockOS_AppStore))
    exec("./BlockOS_AppStore.gui");

function BlockOS_AppStore::onWake(%this) {
    BlockOS_AppStore_Button.setText("Download/Delete");
    BlockOS_AppStore_Button.command = "attemptDownload();";
    ListApps();
}

function AppDesc(%app) {
   if(!isObject(AppStoreHTTP))
           new HTTPObject(AppStoreHTTP);
   BlockOS_AppStore_Load.setVisible(true);
   $BlockOS::AppStore::Line = 0;
   $BlockOS::AppStore::Method = "Desc";
   AppStoreHTTP.get("blockos.nullable.se:80","/store/api/" @ %app @ "?format=blockos");
}

function ListApps() {
   $BlockOS::Store::ErrorSchedule = schedule(6000,0,BlockOS_Error);
   BlockOS_AppStore_Error.setVisible(false);
   BlockOS_AppStore_Load.setVisible(true);
   if(!isObject(AppStoreHTTP))
           new HTTPObject(AppStoreHTTP);

   $BlockOS::AppStore::Line = 0;
   $BlockOS::AppStore::Method = "List";
   BlockOS_AppStore_List.clear();
   AppStoreHTTP.get("blockos.nullable.se:80","/store/api/?format=blockos");
}

function AppStoreHTTP::onLine(%this,%line) {
        if ($BlockOS::AppStore::Line != 0)
        switch$($BlockOS::AppStore::Method) {
                case "Desc":
                        %author = getField(%line,0);
                        %version = getField(%line,1);
                        %name = getField(%line,2);
                        %description = getField(%line,3);
                        %description = strReplace(strReplace(%description, "\\n", "\n"), "\\r", "\r");
                        $BlockOS::Selected::Name = %name;
                        $BlockOS::Selected::Version = %version;
                        BlockOS_AppStore_Name.setValue("Download:" SPC %name SPC "v" @ %version);
                        BlockOS_AppStore_Desc.setValue(%description NL "By " @ %author);
                        BlockOS_AppStore_Load.setVisible(false);
                case "List":
                        %n = $BlockOS::Store::Count++;
                        %name = getField(%line,1);
                        %version = getField(%line,0);
                        %slug = getField(%line,2);
                        $BlockOS::Store::AppName[%name] = %name;
                        $BlockOS::Store::Version[%name] = %version;
                        $BlockOS::Store::Slug[%name] = %slug;
                        BlockOS_AppStore_List.addRow($BlockOS::Store::Count,%name,$BlockOS::Store::Count);
                        BlockOS_AppStore_Load.setVisible(false);
                        cancel($BlockOS::Store::ErrorSchedule);
                        if ($BlockOS::AppReverse[%slug] !$= "" && %version > $BlockOS::AppVersion[$BlockOS::AppReverse[%slug]])
                            messageBoxYesNo("BlockOS", "There is an update available for" SPC %name @ ". Would you like to update?", "attemptDownload(\"" @ %slug @ "\", " @ %version @", true);");
        }
        $BlockOS::AppStore::Line++;
}

function BlockOS_AppStore_List::onSelect(%this,%id,%text) {
        $BlockOS::AppStore::Selected = %text;
        AppDesc($BlockOS::Store::Slug[%text]);
        if(isFile("config/BOS/Apps/" @ $BlockOS::Store::Slug[%text] @ ".zip"))
            BlockOS_AppStore_Button.setText("Delete");
        else
            BlockOS_AppStore_Button.setText("Download");
}

function AttemptDownload(%slug, %version, %forceRestart) {
    %forceRestart += 0;
    if (%slug $= "") {
        %version = $BlockOS::Selected::Version;
        if(BlockOS_AppStore_Button.getText() $= "Download/Delete")
            return messageboxOK("Woops!","You haven't selected an Item yet.");
        %slug = $BlockOS::Store::Slug[$BlockOS::Selected::Name];
        if(BlockOS_AppStore_Button.getText() $= "Delete") {
            messageBoxOK("AppStore","You have deleted " @ $BlockOS::Selected::Name);
            fileDelete("config/BOS/Apps/" @ %slug @ ".zip");
            LoadAppMenu();
            BlockOS_AppStore_Button.setText("Download");
            return;
        } else if(BlockOS_AppStore_Button.getText() $= "Download") {
            BlockOS_AppStore_Button.setText("Delete");
        }
    }

    createDownloaderConnection("store/" @ %slug @ "/" @ %version @ "/download","blockos.nullable.se:80","config/BOS/Apps/" @ %slug @ ".zip", %forceRestart);
}

function BlockOS_Error()
{
   BlockOS_AppStore_Error.setVisible(true);
}
