////////////////////////////
/// BlockOS Skin support ///
////////////////////////////
/// By Teo "Nullable"    ///
////////////////////////////
/// Licensed under the   ///
/// Creative Commons     ///
/// By-NC-SA license     ///
////////////////////////////
/// Don't you dare to    ///
/// remove or edit this  ///
/// header, Brian.       ///
////////////////////////////

if (!$BlockOS::Skinning::Initialized) { // For reload-friendliness
    $BlockOS::Skinning::Initialized = true;
    $BlockOS::Skinning::SkinnableCount = 0;
}

function BlockOS_registerSkinnable(%obj, %kind, %defaultfolder, %name) {
    if (!isObject(%obj)) {
        error("Unable to register a non-object as a skinnable, ignoring.");
        return;
    }
    if (!isFunction(%obj, "setBitmap")) {
        error("Unable to skin an object that is inherently not skinnable, ignoring.");
        return;
    }

    if (%name $= "")
        %name = %obj.getName();
    if (%name $= "") {
        error("BlockOS skinnable registration attempted for object with no name, ignoring.");
        return;
    }

    if ($BlockOS::Skinning::SkinnableForName[%name, "exists"]) {
        error("BlockOS skinnable already exists, ignoring.");
        return;
    }

    if (getSubStr(%defaultfolder, strLen(%defaultfolder) - 1, 1) !$= "/") {
        %defaultfolder = %defaultfolder @ "/";
    }

    switch$ (%kind) {
        case "image":
            if (!isFile(%defaultfolder @ %name @ ".png")) {
                error("Invalid skinnable default file for \"" @ %name @ "\", ignoring.");
                return;
            }
        case "button":
            if (!isFile(%defaultfolder @ %name @ "_d.png")
            &&  !isFile(%defaultfolder @ %name @ "_h.png")
            &&  !isFile(%defaultfolder @ %name @ "_i.png")
            &&  !isFile(%defaultfolder @ %name @ "_n.png")) {
                error("Invalid skinnable default file for \"" @ %name @ "\", ignoring.");
                return;
            }
        default:
            error("Invalid skinnable type \"" @ %kind @ "\", ignoring.");
            return;
    }

    %id = $BlockOS::Skinning::SkinnableCount;
    $BlockOS::Skinning::SkinnableCount++;
    $BlockOS::Skinning::Skinnables[%id] = %name;
    $BlockOS::Skinning::SkinnableForName[%name, "exists"] = true;
    $BlockOS::Skinning::SkinnableForName[%name, "obj"] = %obj;
    $BlockOS::Skinning::SkinnableForName[%name, "kind"] = %kind;
    $BlockOS::Skinning::SkinnableForName[%name, "defaultfolder"] = %defaultfolder;
}

function BlockOS_getBitmapForSkinnable(%skinnable) {
    %defaultfolder = $BlockOS::Skinning::SkinnableForName[%skinnable, "defaultfolder"];
    %skinfolder = "config/BOS/skins/" @ $BlockOS::Save::Skin @ "/";
    %skintestsuffix = ".png";
    switch$ ($BlockOS::Skinning::SkinnableForName[%skinnable, "kind"]) {
        case "button":
            %skintestsuffix = "_i.png";
    }
    if (isFile(%skinfolder @ %skinnable @ %skintestsuffix))
        return %skinfolder @ %skinnable;
    else
        return %defaultfolder @ %skinnable;
}

function BlockOS_setSkin(%skin) {
    if(strLen(%skin) < 1)
    {
       return;
    }
    $BlockOS::Save::Skin = %skin;
    export("$BlockOS::Save*", "Config/BOS/prefs.cs");
    BlockOS_applySkin();
}

function BlockOS_applySkin() {
    for (%i = 0;%i < $BlockOS::Skinning::SkinnableCount;%i++) {
        %skinnable = $BlockOS::Skinning::Skinnables[%i];
        $BlockOS::Skinning::SkinnableForName[%skinnable, "obj"].setBitmap(BlockOS_getBitmapForSkinnable(%skinnable));
    }
}
